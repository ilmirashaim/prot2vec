from src.utils import settings, initLogger

logger = initLogger(settings.newDb.compareResFilename)

oldProts = settings.oldDb.getProteins()
oldNames = oldProts['name']
oldSeq = oldProts['sequence']

def readCsv(db):
   res = db.getProteins(['sequence', 'family', 'entry_name'])
   dataSeq = res['sequence']
   dataNames = res['family']
   dataFamilies = res['entry_name']
   return dataSeq,dataNames,dataFamilies

newSeq,newNames,newFamilies = readCsv(settings.currentDb)
latestSeq,latestNames,latestFamilies = readCsv(settings.newDb)


def analysisOfChangedNames(names1, names2, seq1, seq2, tag1, tag2, year1, year2):
   logger.info('\nAnalysis of changed names')
   # logger.info('Comparison of %s data %s and %s data %s' % (tag1, year1, tag2, year2))
   logger.info('names in %s db whose sequences are in %s db' % (tag1, tag2))

   changedNamesIndices1 = [i for i in xrange(len(names1))
                      if seq1[i] in seq2 and names1[i] not in names2]
   logger.info("number of changed names = %d" % len(changedNamesIndices1))
   for x in changedNamesIndices1:
      ind = seq2.index(seq1[x])
      logger.info("%s name = '%s', %s name = '%s'" % (tag2, names2[ind], tag1, names1[x]))

   logger.info('\nnames in %s db whose sequences are in %s db' % (tag2, tag1))
   changedNamesIndices2 = [i for i in xrange(len(names2))
                      if seq2[i] in seq1 and names2[i] not in names1]
   logger.info("number of changed names = %d" % len(changedNamesIndices2))
   for x in changedNamesIndices2:
      ind = seq1.index(seq2[x])
      logger.info("%s name = '%s', %s name = '%s'" % (tag2, names2[x], tag1, names1[ind]))

def compareNamesAndSeq(names1, names2, seq1, seq2, tag1, tag2, year1, year2):
   logger.info('\n\n\n--------------------------------------------------')
   logger.info('Comparison of %s data %s and %s data %s' % (tag1, year1, tag2, year2))
   logger.info('Number of %s names in %s names %d' % (tag1,tag2,sum([i in names1 for i in names2])))
   logger.info('Number of %s names in %s names %d' % (tag2,tag1,sum([i in names2 for i in names1])))
   logger.info('Number of %s sequences in %s sequences %d' % (tag1,tag2,sum([i in seq1 for i in seq2])))
   logger.info('Number of %s sequences in %s sequences %d' % (tag2,tag1,sum([i in seq2 for i in seq1])))
   analysisOfChangedNames(names1, names2, seq1, seq2, tag1, tag2, year1, year2)

compareNamesAndSeq(newNames, oldNames, newSeq, oldSeq, settings.currentDbLab, settings.oldDbLab, '2015', '2011')
compareNamesAndSeq(newNames, latestNames, newSeq, latestSeq, settings.currentDbLab, settings.newDbLab, '2015', '2016')
compareNamesAndSeq(latestNames, oldNames, latestSeq, oldSeq, settings.newDbLab, settings.oldDbLab, '2016', '2011')

