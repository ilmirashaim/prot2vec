from src.utils import settings
file = "AA.csv"

vectors = []
with open(file, "r") as f:
   for line in f:
      vectors.append(line)
db = settings.currentDb
dict = db.getFamiliesDict()
res = db.getProteins(['sequence', 'name', 'family'], eliminateOrphans = True)
families = [dict[x] for x in res['family']]

distinctFamilies = list(set(families))
print(distinctFamilies)

rightOrder = ['Metabotropic glutamate receptors', 'Calcium-sensing receptors', 'GABA<sub>B</sub> receptors', 'Taste 1 receptors' ]
vectorsNew = ['' for v in xrange(len(vectors))]

j = 0
for fam in rightOrder:
   # search for all protein indices in proteins
   indices = [i for i in xrange(len(families)) if families[i]==fam]
   for i in indices:
      vectorsNew[i] = vectors[j]
      j+=1

with (open(file+"_new", "w")) as f:
   f.write(''.join(vectorsNew))