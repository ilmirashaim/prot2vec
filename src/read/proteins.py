from scipy.io import loadmat as loadmat
import csv

def getProteins(filename, names,
                eliminateOrphans,
                orphanLabel,
                unwrapMglur, mglurLabel):
    res = {}
    for name in names:
        res[name] = []
    indices = {}
    with open(filename, 'rb') as csvfile:
       reader = csv.reader(csvfile, delimiter=',')
       colNames = next(reader)
       famInd = colNames.index('family')
       for name in names:
          indices[name] = (colNames.index(name))
       for val in reader:
          familyLabel = val[famInd][0:11]
          if unwrapMglur and familyLabel == mglurLabel:
              familyLabel = val[famInd]

          if(not eliminateOrphans or (eliminateOrphans and familyLabel != orphanLabel)):
              for name in names:
                 if indices[name] != famInd:
                    res[name].append(val[indices[name]])
                 else:
                    res[name].append(familyLabel)
    return res



def getOldProteins(dbFile, eliminateOrphans, orphanLabel):
    oldDB = loadmat(dbFile, squeeze_me=True, struct_as_record=False)['data']
    oldSeq = oldDB.inputs
    oldHeader = oldDB.Header
    length = len(oldHeader)
    if eliminateOrphans:
        indices = [i for i in xrange(length) if oldDB.targets[i] != orphanLabel]
        names = [oldHeader[i].split(' ')[0] for i in indices]
        sequences = [str(oldSeq[i]).strip() for i in indices]
        families = [oldDB.targets[i] for i in indices]
    else:
        names = [x.split(' ')[0] for x in oldHeader]
        families = oldDB.targets
        sequences = [str(x).strip() for x in oldSeq]
    return {'name':names,'family':families,'sequence':sequences}