import csv
def flattenjson( b, delim ):
    val = {}
    for i in b.keys():
        if isinstance( b[i], dict ):
            get = flattenjson( b[i], delim )
            for j in get.keys():
                val[ i + delim + j ] = get[j]
        else:
            val[i] = b[i]

    return val

def json2csv(input, csvFile):
    input = map( lambda x: flattenjson(x, ','), input )
    columns = map( lambda x: x.keys(), input )
    columns = reduce( lambda x,y: x+y, columns )
    columns = list( set( columns ) )

    with open( csvFile, 'wb' ) as out_file:
        csv_w = csv.writer( out_file , lineterminator='\n')
        csv_w.writerow( columns )

        for i_r in input:
            csv_w.writerow( map( lambda x: i_r.get( x, "" ), columns ) )