import re
filename = "/home/Ilmira.Terpugova/Downloads/uniprot_sprot.dat"

with open(filename, "r") as db:
    with open(filename + ".rearr", "w") as rearr:
        r = ""
        isReading = False
        for line in db:


            if isReading:
                if line.startswith("//"):
                    isReading = False
                    rearr.write(r + '\n')
                    r = ""
                else:
                    r += re.sub("\s", "", line)

            if line.startswith("SQ   SEQUENCE"):
                isReading = True