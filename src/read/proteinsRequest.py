import requests
from src.utils import settings
from jsonUtils import json2csv

slugKey = 'slug'
familiesFilename = settings.currentDb.familiesFilename
proteinsFilename = settings.currentDb.proteinsFilename

def getChildren(families):
   if len(families) == 0:
      return []
   res = []
   for f in families:
      slug = f['slug']
      response = requests.get(root + familyChildrenUrl + slug + "/")
      children = response.json()
      res.extend(children)
      res.extend(getChildren(children))
   return res

root = "http://gpcrdb.org/services/"

# get families of class 'C' recursively
# save in 'families.csv'
familySlug = '004'
familyChildrenUrl = 'proteinfamily/children/'
familyUrl = 'proteinfamily/'


response = requests.get(root + familyUrl + familySlug + "/")
familyData = [response.json()]

familyData.extend(getChildren(familyData))
json2csv(familyData, familiesFilename)
print "Families have been collected to '%s'" % familiesFilename


# get proteins of class 'C'
# save in 'proteins.csv'
proteinUrl = 'proteins/'

response = requests.get(root + familyUrl + proteinUrl + familySlug + "/")
proteinData = response.json()

json2csv(proteinData, proteinsFilename)
print "Proteins have been collected to '%s'" % proteinsFilename

# f= open("../family_classification_metadata.tab")
# for line in f.readlines():
#    l = line.lower()
#    for protein in proteinData:
#       if protein['entry_name'].lower() in l:
#          print('protein %s found\n' % protein['entry_name'])
#
# f.close()

