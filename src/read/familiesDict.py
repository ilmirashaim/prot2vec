import numpy as np

def getFamiliesDict(familiesFilename):
    namesDict = {}
    families = np.recfromcsv(familiesFilename, skip_header=1)
    for family in families:
        namesDict[family[3]] = family[1]
    return namesDict

def getOldFamiliesDict():
    return {1:'MGlu', 2:'CS', 3:'GB', 4:'VN', 5:'Ph', 6:'Od', 7:'Ta', 8:'Orph'}