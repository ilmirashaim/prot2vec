import csv
from itertools import compress
from src.utils import settings

def getVectors(vectorFile, delimiter='\t'):
   XFull = []
   with open(vectorFile, 'rb') as csvfile:
       reader = csv.reader(csvfile, delimiter=delimiter)
       for val in reader:
         XFull.append([float(x) for x in val])
   return XFull

def prepareVectors(db,
                   vectorFile,
                   vectorsFiltered,
                   elimOrphans = settings.elimOrphans,
                   unwrapMglur = settings.unwrapMglu,
                   fileWithOrphans = True,
                   delimiter='\t'):
   print('reading from %s' % vectorFile)

   res = db.getProteins(['family', 'accession'], unwrapMglur=unwrapMglur,
                           eliminateOrphans=elimOrphans if vectorsFiltered else False)
   yLabelsFull = res['family']
   if db.name == settings.oldDbLab:
      accessionsFull = res['name']
   else:
      accessionsFull = res['accession']
   if vectorFile != '':
      XFull = getVectors(vectorFile, delimiter=delimiter)
   else:
      XFull = range(len(yLabelsFull))
   yUnique = list(set(yLabelsFull))

   if elimOrphans and db.orphanLabel in yUnique:
      yUnique.remove(db.orphanLabel)
      isOrphan = [l != db.orphanLabel for l in yLabelsFull]
      # delete orphans from classification
      yLabels = list(compress(yLabelsFull, isOrphan))
      if fileWithOrphans:
         X = list(compress(XFull, isOrphan))
      else:
         X = XFull
      accessions = list(compress(accessionsFull, isOrphan))
   else:
      yLabels = yLabelsFull
      X = XFull
      accessions = accessionsFull


   yDict = dict.fromkeys(yUnique, 0)
   yLabelsUnique = range(len(yUnique))
   yDictReverse = dict.fromkeys(yLabelsUnique, 0)

   for i in yLabelsUnique:
      yDict[yUnique[i]] = i
      yDictReverse[i] = yUnique[i]

   y = []
   for yLabel in yLabels:
      y.append(yDict[yLabel])

   return X, y, accessions, yDictReverse
