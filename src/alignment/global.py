from Bio import pairwise2
from src.utils import settings
import numpy as np
import datetime

db = settings.db
res = db.getProteins(['sequence'])
sequences = res['sequence']

size = len(sequences)
kernel_matrix = np.zeros((size, size))
for i in xrange(size):
   for j in xrange(i+1, size):
      print(datetime.datetime.now())
      kernel_matrix[i][j] = pairwise2.align.globalxx(sequences[i], sequences[j], score_only=True)
      print(i,j,kernel_matrix[i,j])
np.save(db.kernelGlobal, kernel_matrix)
