from src.utils import settings

res = settings.currentDb.getProteins(['accession', 'family', 'sequence'], eliminateOrphans=True)
accessions = res['accession']
familiesIds = res['family']
sequences = res['sequence']

familiesDict = settings.currentDb.getFamiliesDict()
fastaLen = 80

with open(settings.currentDb.fastaFilename, 'w') as fastaFile:
   for i in xrange(len(accessions)):
      fastaFile.write('>' + accessions[i] + ' ' + familiesDict[familiesIds[i]] + '\n')
      fastaFile.write('\n'.join([sequences[i][j:j+fastaLen] for j in range(0, len(sequences[i]), fastaLen)]) + '\n')
print("%s created" % settings.currentDb.fastaFilename)