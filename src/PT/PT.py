from src.utils import settings

res = settings.currentDb.getProteins(['accession', 'family'], eliminateOrphans=True)
accessions = res['accession']
familiesIds = res['family']

with open(settings.currentDb.ptResFilename, 'w') as ptResFile:
   ptResFile.write(','.join(accessions))


familiesDict = settings.currentDb.getFamiliesDict()
colors = ['#4e3149', '#a589a1', '#c5af99', '#8aa234', '#3a5d01']
colorsDict = dict.fromkeys(set(familiesIds))
i = 0
for k in colorsDict:
   colorsDict[k] = colors[i]
   i+=1
print(colorsDict)
print([(x,familiesDict[x]) for x in colorsDict.keys()])
with open(settings.currentDb.ptResAnnotatedFilename, 'w') as ptResFile:
   for i in xrange(len(accessions)):
      ptResFile.write(','.join([accessions[i], colorsDict[familiesIds[i]], familiesDict[familiesIds[i]]]))
      ptResFile.write('\n')

print("%s created" % settings.currentDb.ptResAnnotatedFilename)