from sklearn.model_selection import KFold, GridSearchCV
import sklearn.metrics as metrics
from sklearn.multiclass import OneVsOneClassifier
from sklearn import svm
from src.utils import settings, initLogger, permuteData
import numpy as np
import gensim
from operator import add
from metrics import scoresToStringSubcl, \
   compute_scores_subcl, \
   computeAvgs
import os


class UniprotSequences(object):
   filename = "/home/Ilmira.Terpugova/Downloads/uniprot_sprot.dat.rearr"

   ngramSize = 3 # trigram model


   def splitWithOffset(self, seq, offset):
      res = []
      length = len(seq)
      for i in xrange(offset, length, self.ngramSize):
         if i + self.ngramSize <= length:
            res.append(seq[i:i+self.ngramSize])
      return res


   def __iter__(self):
      with open(self.filename, "r") as file:
          for seq in file:
             for i in xrange(self.ngramSize):
                yield self.splitWithOffset(seq, i)

def gridSearch(clf_orig, X_orig, y_orig, logger):

   scores = [('f1_weighted','f1_weighted')]
   gridSearchClf = None
   for score in scores:
      gridSearchClf = GridSearchCV(clf_orig, paramGrid, scoring=score[1], cv=5)
      gridSearchClf.fit(np.array(X_orig), y_orig)
   #
      logger.info("\n%s %f" % (score[0], gridSearchClf.best_score_))
      logger.info(("best params for score %s" % score[0]) + str(gridSearchClf.best_params_))
   #
   # Select best parameters for accuracy score
   C = gridSearchClf.best_params_['estimator__C']
   gamma = gridSearchClf.best_params_['estimator__gamma']
   clf = OneVsOneClassifier(svm.SVC(gamma=gamma, kernel="rbf", C = C))

   return clf

def computeCrossvalScore(X, y, numFolds = 5, logger=None):
   kf = KFold(n_splits=numFolds)
   scores_list = [[] for i in xrange(nScores)]
   numClasses = len(set(y))

   for train, test in kf.split(X):

      clf_orig = OneVsOneClassifier(svm.SVC(kernel="rbf"))
      Xtrain = X[train]
      Xtest = X[test]

      clf = gridSearch(clf_orig, Xtrain, y[train], logger)
      # clf = clf_orig

      clf.fit(Xtrain, y[train])
      yPred = clf.predict(Xtest)
      scores = compute_scores_subcl(y[test], yPred, numClasses)
      for i in range(nScores):
         scores_list[i].append(scores[i])
      logger.info(scoresToStringSubcl(scores, nScores, scoreNames))
      logger.info("\n" + str(metrics.confusion_matrix(y[test], yPred)))

   avgs = computeAvgs(scores_list, numFolds)
   logger.info("avg metrcis =" +
               scoresToStringSubcl(avgs, nScores, scoreNames))
   # logger.info(confIntToReportString(confInts, nScores))
   return avgs
def prepareVectors(X,
                   yLabels):

   yUnique = list(set(yLabels))


   yDict = dict.fromkeys(yUnique, 0)
   yLabelsUnique = range(len(yUnique))
   yDictReverse = dict.fromkeys(yLabelsUnique, 0)

   for i in yLabelsUnique:
      yDict[yUnique[i]] = i
      yDictReverse[i] = yUnique[i]

   y = []
   for yLabel in yLabels:
      y.append(yDict[yLabel])

   return X, y, yDictReverse


def doClassification(vectors, labels, logger = None):
   XOrig, yOrig, yDictReverse = prepareVectors(vectors, labels)
   XOrig = np.array(XOrig)
   yOrig = np.array(yOrig)
   XOrig,yOrig,ind = permuteData(XOrig,yOrig)
   print(yDictReverse)
   print(set(yOrig))
   familiesDict = db.getFamiliesDict()
   for k in yDictReverse.values():
      print k, familiesDict[k]
   assert len(XOrig) == len(yOrig)


   X = XOrig
   avgs = computeCrossvalScore(X,
                        yOrig,
                        numFolds = 5,
                        logger=logger)
   return avgs


def getSequenceVectors(sequences, ngramsDict, ngramSize, vecSize, logger):
        defaultVector = [0]*vecSize
        seqVectors = []
        totalN = 0
        notFound = 0

        for sequence in sequences:
            seqVector = defaultVector
            for j in xrange(len(sequence) - ngramSize + 1):
                vec = ngramsDict.get(sequence[j:j+ngramSize], defaultVector)
                if all(vec[i] == defaultVector[i] for i in range(len(vec))):
                    notFound+=1
                totalN +=1
                seqVector = map(add, seqVector, vec)
            seqVectors.append(seqVector)
        logger.info("Number of not found %d out of %d" % (notFound, totalN))
        return seqVectors


if __name__ == "__main__":
   paramGrid = {
      'estimator__C': [1,2,3,4,5, 50,100,1000, 10000],
      'estimator__gamma': [0.1, 0.01, 0.001, 0.0001, 0.00001, 1e-06, 1e-07]

   }

   eliminateOrphans = settings.elimOrphans

   db = settings.oldDb

   nScores = 7 # number of different scores
   scoreNames = ['acc', 'MCC', 'f-score', 'precision_subcl', 'recall_subcl', 'f-score_subcl', 'MCC_subcl']

   windows = [5, 10, 20, 25, 30, 40]
   ngramSizes = [3, 4, 2, 5]
   sizes = [100, 200, 300]
   sgs = [1, 0]
   logger = initLogger(db.classificationResFilename + "comp_unipot5")

   res = db.getProteins(['sequence', 'name', 'family'],
                          unwrapMglur=False,
                          eliminateOrphans= eliminateOrphans)

   sequences = res['sequence']
   names = res['name']
   families = res['family']


   for ngramSize in ngramSizes:
      for size in sizes:
         for sg in sgs:
            for window in windows:
               # if ngramSize == 3:
               #    continue
               # if ngramSize == 4 and sg==1 and size==100 and window<=30:
               #    continue
               logger.info("window = %d, sg = %d, ngramSize = %d, size = %d" % (window, sg, ngramSize, size))

               sentences = UniprotSequences() # a memory-friendly iterator
               sentences.ngramSize = ngramSize
               filePath = settings.currentDb.modelFilename + "uniprot_sg%d_size%d_n%d_window%d" % (sg,size,ngramSize,window)
               if os.path.exists(filePath):
                  model = gensim.models.Word2Vec.load(filePath)
               else:
                  model = gensim.models.Word2Vec(sentences, sg=sg, min_count=2, window=window, size=size)
                  model.save(filePath)
               ngramsDict = {}


               keys = model.vocab.keys()
               for k in keys:
                  ngramsDict[k] = model[k]
               vectors = getSequenceVectors(sequences, ngramsDict, ngramSize, size, logger)


               r = doClassification(vectors,
                                      families,
                                      logger=logger)