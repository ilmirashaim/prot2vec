from sklearn.multiclass import OneVsOneClassifier, _predict_binary
from sklearn.utils.validation import check_is_fitted
import numpy as np

class ExtendedOneVsOneClassifier(OneVsOneClassifier):
   def getVotesAndCF(self, X):
        """Decision function for the OneVsOneClassifier.
        Returns the value of votes and sum of confidence levels for each example

        Parameters
        ----------
        X : array-like, shape = [n_samples, n_features]

        Returns
        -------
        Y : array-like, shape = [n_samples, n_classes]
        """
        check_is_fitted(self, 'estimators_')

        n_samples = X.shape[0]
        n_classes = self.classes_.shape[0]
        votes = np.zeros((n_samples, n_classes))
        K = n_classes*(n_classes-1)/2
        confidence_levels = np.zeros((n_samples, K))

        k = 0
        for i in range(n_classes):
            for j in range(i + 1, n_classes):
                pred = self.estimators_[k].predict(X)
                confidence_levels[:,k] = _predict_binary(self.estimators_[k], X)
                votes[pred == 0, i] += 1
                votes[pred == 1, j] += 1
                k += 1
        return votes, confidence_levels
