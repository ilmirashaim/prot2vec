from sklearn.model_selection import KFold
import sklearn.metrics as metrics
from sklearn.naive_bayes import GaussianNB
from src.read.vectors import  prepareVectors
from src.utils import settings, initLogger, permuteData
import numpy as np
from metrics import scoresToStringSubcl, \
   compute_scores_subcl, \
   computeAvgs


def computeCrossvalScore(X, y, numFolds = 5, logger=None):
   kf = KFold(n_splits=numFolds)
   scores_list = [[] for i in xrange(nScores)]
   numClasses = len(set(y))
   X,y,ind = permuteData(X,y)

   for train, test in kf.split(X):

      clf = GaussianNB()
      Xtrain = X[train]
      print(len(Xtrain))
      Xtest = X[test]
      print(len(Xtest))


      clf.fit(Xtrain, y[train])
      yPred = clf.predict(Xtest)
      scores = compute_scores_subcl(y[test], yPred, numClasses)
      for i in range(nScores):
         scores_list[i].append(scores[i])
      logger.info(scoresToStringSubcl(scores, nScores, scoreNames))
      logger.info("\n" + str(metrics.confusion_matrix(y[test], yPred)))

   avgs = computeAvgs(scores_list, numFolds)
   logger.info("avg metrcis =" +
               scoresToStringSubcl(avgs, nScores, scoreNames))
   return avgs

def doClassification(modelName, vectorFile, isFileWithOrphans=True, logger = None, delimiter='\t'):
   logger.info("\nModel %s" % modelName)

   XOrig, yOrig, names, yDictReverse = prepareVectors(db, vectorFile,
                                                      False,
                                                      elimOrphans=eliminateOrphans,
                                                      unwrapMglur=settings.unwrapMglu,
                                                      fileWithOrphans=isFileWithOrphans,
                                                      delimiter =delimiter)
   XOrig = np.array(XOrig)
   yOrig = np.array(yOrig)

   print(yDictReverse)
   print(set(yOrig))
   familiesDict = db.getFamiliesDict()

   for k in yDictReverse.values():
      print k, familiesDict[k]

   assert len(XOrig) == len(yOrig)


   X = XOrig


   avgs = computeCrossvalScore(X,
                        yOrig,
                        numFolds = 5,
                        logger=logger)
   return avgs



if __name__ == "__main__":

   eliminateOrphans = settings.elimOrphans

   db = settings.oldDb

   nScores = 7 # number of different scores
   scoreNames = ['acc', 'MCC', 'f-score', 'precision_subcl', 'recall_subcl', 'f-score_subcl', 'MCC_subcl']

   vectorFiles = [db.aaVectorsFilename, db.swissprotVectorsFilename, db.gpcrdbVectorsFilename,
                   db.digramVectorsFilename, db.accVectorsFilename]
   isFileWithOrphans = [False, True, True, False, True]
   delimiters = [',', '\t', '\t', ',', '\t']
   modelNames = ['AA'] + db.modelNames + ['Digram', 'ACC']

   logger = initLogger(db.classificationResNBFilename)
   avgs = {}
   for k in xrange(len(vectorFiles)):
      res = doClassification(modelNames[k],
                             vectorFiles[k],
                             isFileWithOrphans=isFileWithOrphans[k],
                             logger=logger,
                             delimiter=delimiters[k])
      avgs[modelNames[k]] = res

   print avgs