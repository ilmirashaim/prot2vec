from sklearn import svm
from sklearn.model_selection import KFold, GridSearchCV, StratifiedShuffleSplit
from sklearn.multiclass import OneVsOneClassifier
import sklearn.metrics as metrics
from src.read.vectors import  prepareVectors
from src.utils import settings, initLogger
import numpy as np
from metrics import multiclassMCC, scoresToString,\
   scoresToReportString, compute_scores, \
   confIntToString, confIntToReportString
import scipy.stats as stats


def mean_confidence_interval(data, confidence=0.95):
    n = len(data)
    m, se = np.mean(data), stats.sem(data)
    h = se * stats.t._ppf((1+confidence)/2., n-1)
    return m, h

nFolds = 5
paramGrid = {'estimator__C': [1,2,3,4,5, 100, 4279220.16952312],
              'estimator__gamma': [0.1, 0.01, 0.001, 0.0001, 0.00001, 1e-08]}
# paramGrid = {'estimator__C': [1],
#               'estimator__gamma': [0.1]}

eliminateOrphans = settings.elimOrphans
tsneClassification = settings.tsneClassification
usePca = settings.pca

db = settings.db

logger = initLogger(db.classificationResFilename + "FULL")

nScores = 4 # number of different scores
scoreNames = ['MCC', 'acc', 'precision', 'recall']


if not tsneClassification:
   vectorFiles = [db.swissprotVectorsFilename, db.gpcrdbVectorsFilename,
                  db.aacVectorsFilename, db.accVectorsFilename]
else:
   vectorFiles = [db.swissprotVectorsTsneFilename, db.gpcrdbVectorsTsneFilename,
                  db.aacVectorsFilename, db.accVectorsFilename]
proteinsFile = db.proteinsFilename
orphanLabel = db.orphanLabel
familiesDict = db.getFamiliesDict()
modelNames = db.modelNames + ["AAC", "ACC"]

def gridSearch(clf_orig, X_orig, y_orig, kernel):
   #First perform a grid search to choose gamma and C

   def mcc_scorer(y_true, y_pred):
      cm = metrics.confusion_matrix(y_true, y_pred)
      return multiclassMCC(cm)
   #
   scores = [('accuracy','accuracy'), ('mcc', metrics.make_scorer(mcc_scorer))]
   gridSearchClf = None
   for score in scores:
      gridSearchClf = GridSearchCV(clf_orig, paramGrid, scoring=score[1], cv=3)
      gridSearchClf.fit(np.array(X_orig), y_orig)
   #
      logger.info("\n%s %f" % (score[0], gridSearchClf.best_score_))
      logger.info(("best params for score %s" % score[0]) + str(gridSearchClf.best_params_))
   #
   # Select best parameters for mcc score
   C = gridSearchClf.best_params_['estimator__C']
   gamma = gridSearchClf.best_params_['estimator__gamma']
   clf = svm.SVC(gamma=gamma, kernel=kernel, C = C)
   return clf

def computeCrossvalScore(clf, X, y, numFolds = 5, kernel="rbf"):
   kf = KFold(n_splits=numFolds)
   scores_list = [[] for i in xrange(nScores)]


   for train, test in kf.split(X):
      if kernel =="precomputed":
         Xtrain = X[train,:][:,train]
         Xtest = X[test,:][:,train]
      else:
         Xtrain = X[train]
         Xtest = X[test]
      clf.fit(Xtrain, y[train])
      yPred = clf.predict(Xtest)
      scores = compute_scores(y[test], yPred)
      for i in range(nScores):
         scores_list[i].append(scores[i])
      logger.info(scoresToString(scores, nScores, scoreNames))
      logger.info("\n" + str(metrics.confusion_matrix(y[test], yPred)))

   confInts = [mean_confidence_interval(s) for s in scores_list]
   logger.info("conf int = " +
               confIntToString(confInts, nScores, scoreNames))
   logger.info(confIntToReportString(confInts, nScores))
   logger.info(scoresToReportString([s[0] for s in scores_list], nScores))
   return confInts

def doClassification(modelName, vectorFile, kernel="rbf", matrix = None):
   logger.info("\nModel %s" % modelName)

   XOrig, yOrig, names, yDictReverse = prepareVectors(db, vectorFile,
                                                      tsneClassification,
                                                      eliminateOrphans,
                                                      settings.unwrapMglu)
   # if kernel =="precomputed":
   #    XOrig = range(len(yOrig))
   XOrig = np.array(XOrig)
   yOrig = np.array(yOrig)
   splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=42)
   trainInd, testInd = splitter.split(XOrig, yOrig).next()

   clf_orig = OneVsOneClassifier(svm.SVC(kernel=kernel))


   if kernel =="precomputed":
      Xtrain = matrix[trainInd,:][:,trainInd]
      Xtest = matrix[testInd,:][:,trainInd]
   else:
      Xtrain = XOrig[trainInd]
      Xtest = XOrig[testInd]
   clf = gridSearch(clf_orig, Xtrain, yOrig[trainInd], kernel)
   # clf = OneVsOneClassifier(clf)
   confInts = computeCrossvalScore(clf, Xtrain,
                        yOrig[trainInd],
                        numFolds = 10,
                        kernel=kernel)
   clf.fit(Xtrain, yOrig[trainInd])
   yPred = clf.predict(Xtest)
   scores = compute_scores(yOrig[testInd], yPred)
   logger.info(scoresToString(scores, nScores, scoreNames))

   logger.info("\n" + str(metrics.confusion_matrix(yOrig[testInd], yPred)))
   return confInts


def saveConfInts(confInts, scoreNames):
   for i in xrange(len(scoreNames)):
      fileName = db.confInts + "_" + scoreNames[i]
      with open(fileName, "w") as file:
         for x in confInts:
            file.write("%s\n"%x)
            file.write(",".join(['%.4f'% z for z in confInts[x][i]]) + "\n")



if __name__ == "__main__":
   confInts = {}
   for k in xrange(len(vectorFiles)):
      res = doClassification(modelNames[k], vectorFiles[k])
      confInts[modelNames[k]] = res
   kernelFile = db.kernelGlobal+'.npy'
   logger.info("kernel file %s" % kernelFile)
   K = np.load(kernelFile)
   R = np.zeros((len(K), len(K)))
   for i in range(len(K)):
      for j in range(i+1, len(K)):
         R[i][j] = K[i][j]
         R[j][i] = K[i][j]
   modelName = "alignment"
   res = doClassification(modelName, "", kernel="precomputed", matrix = R)
   confInts[modelName] = res

   saveConfInts(confInts, scoreNames)












