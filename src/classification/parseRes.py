import re
file = "/Users/Ilmira/Study/thesis/Project/proteins/resources/results/new/classificationRes.txtdoc2vec"
results_strs = []
models = []
with open(file, "r") as f:
   for line in f:
      if "avg metrcis" in line:
         results_strs.append(line)
      if "Model " in line:
         models.append(line[6:-1])
k=0
# res_str = "avg metrcis =acc = 0.9893, MCC = 0.9830, f-score = 0.9891, precision_subcl = 1.0000,0.9888,0.9844,1.0000, recall_subcl = 0.9251,0.9951,0.9978,0.9862, f-score_subcl = 0.9593,0.9918,0.9910,0.9930, MCC_subcl = 0.9580,0.9896,0.9811,0.9917"
for res_str in results_strs:
   res_str = res_str[26:]
   res = re.sub("[a-zA-Z=\s_-]", "", res_str)
   splits = res.split(",")

   print splits

   res_out = """\\begin{table}[!htb]
   \centering
   \label{tbl:res}
   \\begin{tabular}{|l|l|l|l|}
   \hline
   Model &  Accuracy & MCC & F-measure \\\\\\hline
   Swiss-Prot &  $0$ & $1$ &  $2$ \\\\\\hline
   \end{tabular}
   \end{table}

   \\begin{table}[!htb]
   \centering
   \label{tbl:res}
   \\begin{tabular}{|l|l|l|l|l|}
   \hline
   Class & Precision & Recall  & F-measure & MCC\\\\\\hline
   MGlur & $5$ & $9$ & $13$ & $17$ \\\\\\hline
   CS & $6$ & $10$ & $14$ & $18$ \\\\\\hline
   GABA\_B & $3$ & $7$ & $11$ &  $15$ \\\\\\hline
   Taste 1 & $4$ & $8$ & $12$ &  $16$ \\\\\\hline
   \end{tabular}
   \end{table}"""

   for i,s in enumerate(splits):
      res_out = re.sub("\$"+str(i)+"\$", str(s), res_out)
   res_out = re.sub("Swiss-Prot ", models[k], res_out)
   print res_out
   k+=1