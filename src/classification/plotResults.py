import matplotlib.pyplot as plt
from src.utils import settings
import matplotlib.patches as mpatches

dbs = [settings.newDb, settings.currentDb, settings.oldDb]

scoreNames = ['MCC', 'acc', 'precision', 'recall']
def readConfInts(db, scoreNames):
   confInts = {}
   for i in xrange(len(scoreNames)):
      fileName = db.confInts + "_" + scoreNames[i]
      confInts[scoreNames[i]] = {}
      with open(fileName, "r") as file:
         name = file.readline().strip()
         while name:
            res = [float(x) for x in file.readline().split(",")]
            confInts[scoreNames[i]][name] = res
            name = file.readline().strip()
   return confInts



def getConfIntsForScore(confInts, scoreName, offset=0.0):
   modelNames = [x.strip() for x in confInts[scoreName].keys()]
   x = [a+offset for a in range(len(confInts[scoreName]))]
   y = [confInts[scoreName][m][0] for m in modelNames]
   yerr = [confInts[scoreName][m][1] for m in modelNames]
   return x, y, yerr

def plotScore(confInts, ax, scoreName, title, color, offset=0.0):
   modelNames = [x.strip() for x in confInts[scoreName].keys()]
   x, y, yerr = getConfIntsForScore(confInts, scoreName, offset)
   ax.errorbar(x, y, yerr=yerr, fmt='o', color=color)
   ax.set_title(title)
   ax.set_xticklabels([''] + modelNames + [''], rotation=90)
   ax.set_ylim((0.75, 1.006))
   ax.set_xlim((-1, len(confInts[scoreName])))

confIntsNew = readConfInts(dbs[0], scoreNames)
confIntsCur = readConfInts(dbs[1], scoreNames)
confIntsOld = readConfInts(dbs[2], scoreNames)

plt.figure()
fig, axs = plt.subplots(nrows=2, ncols=2, sharex=True)

ax = axs[0][0]
plotScore(confIntsOld, ax, 'MCC', 'MCC', 'green', -0.1)
plotScore(confIntsCur, ax, 'MCC', 'MCC', 'b', 0.0)
plotScore(confIntsNew, ax, 'MCC', 'MCC', 'r', 0.1)
ax = axs[0][1]
plotScore(confIntsOld, ax, 'acc', 'Accuracy', 'green', -0.1)
plotScore(confIntsCur, ax, 'acc', 'Accuracy', 'b', 0.0)
plotScore(confIntsNew, ax, 'acc', 'Accuracy', 'r', 0.1)
ax = axs[1][0]
plotScore(confIntsOld, ax, 'precision', 'Precision', 'green', 0.1)
plotScore(confIntsCur, ax, 'precision', 'Precision', 'b', 0.0)
plotScore(confIntsNew, ax, 'precision', 'Precision', 'r', 0.1)
ax = axs[1][1]
plotScore(confIntsOld, ax, 'recall', 'Recall', 'green', -0.1)
plotScore(confIntsCur, ax, 'recall', 'Recall', 'b', 0.0)
plotScore(confIntsNew, ax, 'recall', 'Recall', 'r', 0.1)

red_patch = mpatches.Patch(color='red', label='September 2016 data')
blue_patch = mpatches.Patch(color='blue', label='May 2016 data')
green_patch = mpatches.Patch(color='green', label='2011 data')
plt.legend(handles=[red_patch, blue_patch, green_patch],bbox_to_anchor=(1, 0.3), loc=1, borderaxespad=0.)

fig.suptitle('Models scores')
plt.show()