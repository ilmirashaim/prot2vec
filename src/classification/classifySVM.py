from sklearn.model_selection import KFold, GridSearchCV
import sklearn.metrics as metrics
from sklearn.multiclass import OneVsOneClassifier
from sklearn import svm
from src.read.vectors import  prepareVectors
from src.utils import settings, initLogger, permuteData
import numpy as np
from metrics import scoresToStringSubcl, \
   compute_scores_subcl, \
   computeAvgs


def gridSearch(clf_orig, X_orig, y_orig, logger):

   scores = [('f1_weighted','f1_weighted')]
   gridSearchClf = None
   for score in scores:
      gridSearchClf = GridSearchCV(clf_orig, paramGrid, scoring=score[1], cv=5)
      gridSearchClf.fit(np.array(X_orig), y_orig)
   #
      logger.info("\n%s %f" % (score[0], gridSearchClf.best_score_))
      logger.info(("best params for score %s" % score[0]) + str(gridSearchClf.best_params_))
   #
   # Select best parameters for accuracy score
   C = gridSearchClf.best_params_['estimator__C']
   gamma = gridSearchClf.best_params_['estimator__gamma']
   clf = OneVsOneClassifier(svm.SVC(gamma=gamma, kernel="rbf", C = C))

   return clf

def computeCrossvalScore(X, y, numFolds = 5, logger=None):
   kf = KFold(n_splits=numFolds)
   scores_list = [[] for i in xrange(nScores)]
   numClasses = len(set(y))

   for train, test in kf.split(X):

      clf_orig = OneVsOneClassifier(svm.SVC(kernel="rbf"))
      Xtrain = X[train]
      Xtest = X[test]

      clf = gridSearch(clf_orig, Xtrain, y[train], logger)
      # clf = clf_orig

      clf.fit(Xtrain, y[train])
      yPred = clf.predict(Xtest)
      scores = compute_scores_subcl(y[test], yPred, numClasses)
      for i in range(nScores):
         scores_list[i].append(scores[i])
      logger.info(scoresToStringSubcl(scores, nScores, scoreNames))
      logger.info("\n" + str(metrics.confusion_matrix(y[test], yPred)))

   avgs = computeAvgs(scores_list, numFolds)
   logger.info("avg metrcis =" +
               scoresToStringSubcl(avgs, nScores, scoreNames))
   # logger.info(confIntToReportString(confInts, nScores))
   return avgs

def doClassification(modelName, vectorFile, isFileWithOrphans=True, logger = None, delimiter='\t'):
   logger.info("\nModel %s" % modelName)
   XOrig, yOrig, names, yDictReverse = prepareVectors(db, vectorFile,
                                                      False,
                                                      elimOrphans=eliminateOrphans,
                                                      unwrapMglur=settings.unwrapMglu,
                                                      fileWithOrphans=isFileWithOrphans,
                                                      delimiter =delimiter)
   XOrig = np.array(XOrig)
   yOrig = np.array(yOrig)
   XOrig,yOrig,ind = permuteData(XOrig,yOrig)
   print(yDictReverse)
   print(set(yOrig))
   familiesDict = db.getFamiliesDict()
   for k in yDictReverse.values():
      print k, familiesDict[k]
   assert len(XOrig) == len(yOrig)


   X = XOrig
   avgs = computeCrossvalScore(X,
                        yOrig,
                        numFolds = 5,
                        logger=logger)
   return avgs



if __name__ == "__main__":
   paramGrid = {
      'estimator__C': [1,2,3,4,5, 50,100,1000, 10000],
      'estimator__gamma': [0.1, 0.01, 0.001, 0.0001, 0.00001, 1e-06, 1e-07]

   }

   eliminateOrphans = settings.elimOrphans

   db = settings.newDb

   nScores = 7 # number of different scores
   scoreNames = ['acc', 'MCC', 'f-score', 'precision_subcl', 'recall_subcl', 'f-score_subcl', 'MCC_subcl']

   vectorFiles = [db.doc2vecVectorsFilename]
   isFileWithOrphans = [True]
   delimiters = ['\t']
   modelNames = ['doc2vec']
   # vectorFiles = [db.aaVectorsFilename, db.swissprotVectorsFilename, db.gpcrdbVectorsFilename,
   #                 db.digramVectorsFilename, db.accVectorsFilename]
   # isFileWithOrphans = [False, True, True, False, True]
   # delimiters = [',', '\t', '\t', ',', '\t']
   # modelNames = ['AA'] + db.modelNames + ['Digram', 'ACC']

   logger = initLogger(db.classificationResFilename + "doc2vec")
   avgs = {}
   for k in xrange(len(vectorFiles)):
      res = doClassification(modelNames[k],
                             vectorFiles[k],
                             isFileWithOrphans=isFileWithOrphans[k],
                             logger=logger,
                             delimiter=delimiters[k])
      avgs[modelNames[k]] = res

   print avgs