from sklearn.model_selection import KFold, GridSearchCV
from sklearn.ensemble import RandomForestClassifier
import sklearn.metrics as metrics
from src.read.vectors import  prepareVectors
from src.utils import settings, initLogger
import numpy as np
from metrics import scoresToStringSubcl, \
   compute_scores_subcl, \
   computeAvgs


def gridSearch(clf_orig, X_orig, y_orig, logger):

   scores = [('accuracy','accuracy')]
   gridSearchClf = None
   for score in scores:
      gridSearchClf = GridSearchCV(clf_orig, paramGrid, scoring=score[1], cv=5)
      gridSearchClf.fit(np.array(X_orig), y_orig)
   #
      logger.info("\n%s %f" % (score[0], gridSearchClf.best_score_))
      logger.info(("best params for score %s" % score[0]) + str(gridSearchClf.best_params_))
   #
   # Select best parameters for accuracy score
   n_estimators = gridSearchClf.best_params_['n_estimators']
   max_features = gridSearchClf.best_params_['max_features']
   min_samples_leaf = gridSearchClf.best_params_['min_samples_leaf']
   criterion = gridSearchClf.best_params_['criterion']
   clf = RandomForestClassifier(n_jobs=-1,
                                n_estimators=n_estimators,
                                max_features=max_features,
                                min_samples_leaf=min_samples_leaf,
                                criterion=criterion
   )
   return clf

def computeCrossvalScore(X, y, numFolds = 5, logger=None):
   kf = KFold(n_splits=numFolds)
   scores_list = [[] for i in xrange(nScores)]


   for train, test in kf.split(X):

      clf_orig = RandomForestClassifier(n_jobs=-1)
      Xtrain = X[train]
      Xtest = X[test]

      clf = gridSearch(clf_orig, Xtrain, y[train], logger)

      clf.fit(Xtrain, y[train])
      yPred = clf.predict(Xtest)
      scores = compute_scores_subcl(y[test], yPred)
      for i in range(nScores):
         scores_list[i].append(scores[i])
      logger.info(scoresToStringSubcl(scores, nScores, scoreNames))
      logger.info("\n" + str(metrics.confusion_matrix(y[test], yPred)))

   avgs = computeAvgs(scores_list)
   logger.info("avg metrcis =" +
               scoresToStringSubcl(avgs, nScores, scoreNames))
   # logger.info(confIntToReportString(confInts, nScores))
   return avgs

def doClassification(modelName, vectorFile, isFileWithOrphans=True, logger = None, delimiter='\t'):
   logger.info("\nModel %s" % modelName)
   XOrig, yOrig, names, yDictReverse = prepareVectors(db, vectorFile,
                                                      False,
                                                      elimOrphans=eliminateOrphans,
                                                      unwrapMglur=settings.unwrapMglu,
                                                      fileWithOrphans=isFileWithOrphans,
                                                      delimiter =delimiter)
   XOrig = np.array(XOrig)
   yOrig = np.array(yOrig)
   print(yDictReverse)
   familiesDict = db.getFamiliesDict()
   for k in yDictReverse.values():
      print k, familiesDict[k]
   assert len(XOrig) == len(yOrig)


   X = XOrig
   avgs = computeCrossvalScore(X,
                        yOrig,
                        numFolds = 5,
                        logger=logger)
   return avgs



if __name__ == "__main__":
   paramGrid = {
      'n_estimators': [100, 500, 1000],
      # 'n_estimators': [500],
      # 'criterion': ["gini", "entropy"],
      'criterion': ["gini"],
      'max_features': [0.1, 0.4, 0.7, 1.0],
      'min_samples_leaf': [1, 2, 0.01, 0.05]

   }

   eliminateOrphans = settings.elimOrphans

   db = settings.db

   nScores = 7 # number of different scores
   scoreNames = ['acc', 'MCC', 'f-score', 'precision_subcl', 'recall_subcl', 'f-score_subcl', 'MCC_subcl']

   vectorFiles = [db.swissprotVectorsFilename, db.gpcrdbVectorsFilename,
                  db.aaVectorsFilename, db.digramVectorsFilename, db.accVectorsFilename]
   isFileWithOrphans = [True, True, False, False, True]
   delimiters = ['\t','\t',',', ',', '\t']
   modelNames = db.modelNames + ['AA', 'Digram', 'ACC']

   logger = initLogger(db.classificationResRFFilename)
   avgs = {}
   for k in xrange(len(vectorFiles)):
      res = doClassification(modelNames[k],
                             vectorFiles[k],
                             isFileWithOrphans=isFileWithOrphans[k],
                             logger=logger,
                             delimiter=delimiters[k])
      avgs[modelNames[k]] = res

   print avgs