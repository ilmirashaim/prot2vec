import re
file = "/Users/Ilmira/Study/thesis/Project/proteins/resources/results/new/classificationRes.txtcomp3"
results_strs = []
models = []
with open(file, "r") as f:
   for line in f:
      if "avg metrcis" in line:
         results_strs.append(line)
      if "window = " in line:
         models.append(line[26:-1])
k=0
# res_str = "avg metrcis =acc = 0.9893, MCC = 0.9830, f-score = 0.9891, precision_subcl = 1.0000,0.9888,0.9844,1.0000, recall_subcl = 0.9251,0.9951,0.9978,0.9862, f-score_subcl = 0.9593,0.9918,0.9910,0.9930, MCC_subcl = 0.9580,0.9896,0.9811,0.9917"
for res_str in results_strs:
   res_str = res_str[26:]
   res = re.sub("[a-zA-Z=\s_-]", "", res_str)
   splits = res.split(",")

   print models[k] +" & "+ " & ".join(splits[:3])
   k+=1
