from sklearn import svm
from sklearn.model_selection import KFold, GridSearchCV
from sklearn.neural_network import MLPClassifier
from sklearn.multiclass import OneVsOneClassifier
from sklearn.decomposition import PCA
import sklearn.metrics as metrics
from src.read.vectors import  prepareVectors
from src.utils import settings, initLogger, permuteData
import numpy as np
from metrics import multiclassMCC, scoresToString, compute_scores
from classifier import ExtendedOneVsOneClassifier

nFolds = 3
nRuns = 5

paramGrid = {'estimator__C': [1,2,3,4,5],
              'estimator__gamma': [0.1, 0.01, 0.001, 0.0001, 0.00001]}
eliminateOrphans = settings.elimOrphans
tsneClassification = settings.tsneClassification
usePca = settings.pca

db = settings.db

logger = initLogger(db.classificationResFilename)

nScores = 4 # number of different scores
# gamma = 0.0001
scoreNames = ['MCC', 'acc', 'precision', 'recall']


if not tsneClassification:
   vectorFiles = [db.swissprotVectorsFilename, db.gpcrdbVectorsFilename]
else:
   vectorFiles = [db.swissprotVectorsTsneFilename, db.gpcrdbVectorsTsneFilename]
proteinsFile = db.proteinsFilename
orphanLabel = db.orphanLabel
familiesDict = db.getFamiliesDict()
modelNames = db.modelNames



keys = ["name", "rate", "vt", "vp", "rs", "cdv", "true", "predicted"]

largeCDV = 60
verylargeCDV = 95

mcRateThreshold = 0.75

def sortByFields(arr, fields):
   res = arr
   for f in fields:
      res = sorted(res, key=lambda dict: dict[f])
   return res

def mcToString(mc, yDictReverse):
   strRes = ""
   for k in keys:
      val =mc[k]
      if(k in ["predicted", "true"]):
         val = "\"" + familiesDict[yDictReverse[val]] + "\""
      if(isinstance(val, float)):
         strVal = "%3.3f"%val
      else:
         strVal = str(val)
      strRes = strRes + " " + k + ":" + strVal
   return strRes

def getLinearIndex(class1, class2, nClasses):
   '''
   Computes index of one-vs-one classifer which compares class 1 to class 2
   in  nClasses*(nClasses-1)/2 array
   :param class1: true labels
   :param class2: predicted labels
   :param nClasses: number of classes
   :return:
   '''
   k = 0
   for i in range(nClasses):
      for j in range(i + 1, nClasses):
         if ((class1 == i and class2 == j)
            or (class1 == j and class2 == i)):
            return k
         k+=1
   return k

def computeCdv(cfLevel, y, y_mc, nClasses, nRuns):
   return cfLevel[getLinearIndex(y, y_mc, nClasses)]*100/nRuns

def computeMc(names, y, pred, votes, cfLevels):
   nSamples = len(y)
   nClasses = len(set(y))

   predCorrectCount = [pred[i][y[i]] for i in xrange(nSamples)]

   # count predictions without true class
   predNoCorrectClass = [np.concatenate((pred[i][0:y[i]], [0], pred[i][y[i]+1:]))
                              for i in xrange(nSamples)]
   # most frequent incorrect class
   y_mc = [np.array(p).argmax() for p in predNoCorrectClass]

   mcRate = [(nRuns-predCorrectCount[i])/float(nRuns)
                    for i in xrange(nSamples)]

   logger.info("max misclassification rate %f" % max(mcRate))

   is_mc = [x > mcRateThreshold  for x in mcRate]

   logger.info("number of misclassified is %d" % sum(is_mc))

   mc = [{"name": names[i],
                     "rate": mcRate[i],
                     "vt": votes[i][y[i]],
                     "vp": votes[i][y_mc[i]],
                     "rs": votes[i][y[i]]/votes[i][y_mc[i]],
                     "cdv": computeCdv(cfLevels[i], y[i], y_mc[i],
                                        nClasses, nRuns),
                     "predicted": y_mc[i],
                     "true": y[i]}
                    for i, elem in enumerate(is_mc) if elem]

   logger.info("number of misclassified with voting ratio <= 0.5 (large error) is %d" %
               sum([x['rs']<=0.5 for x in mc]))

   numberOfLargeCdv = sum([abs(x['cdv'])>largeCDV for x in mc])
   numberOfVeryLargeCdv = sum([abs(x['cdv'])>verylargeCDV for x in mc])

   logger.info("number of misclassified with |cdv|>%d (large error) is %d,"
               "\n and with |cdv|>%d (very large error) is %d" %
               (largeCDV, numberOfLargeCdv,
                verylargeCDV, numberOfVeryLargeCdv))

   mc = sortByFields(mc, ["name", "rs"])

   return mc


def gridSearch(clf_orig, X_orig, y_orig):
   #First perform a grid search to choose gamma and C

   def mcc_scorer(y_true, y_pred):
      cm = metrics.confusion_matrix(y_true, y_pred)
      return multiclassMCC(cm)
   #
   scores = [('accuracy','accuracy'), ('mcc', metrics.make_scorer(mcc_scorer))]
   gridSearchClf = None
   for score in scores:
      gridSearchClf = GridSearchCV(clf_orig, paramGrid, scoring=score[1], cv=5)
      gridSearchClf.fit(np.array(X_orig), y_orig)
   #
      logger.info("\n%s %f" % (score[0], gridSearchClf.best_score_))
      logger.info(gridSearchClf.best_params_)
   #
   # Select best parameters for mcc score
   C = gridSearchClf.best_params_['estimator__C']
   gamma = gridSearchClf.best_params_['estimator__gamma']
   clf = svm.SVC(gamma=gamma, kernel="rbf", C = C)
   return clf


def doClassification(modelName, vectorFile):
   logger.info("\nModel %s" % modelName)

   XOrig, yOrig, names, yDictReverse = prepareVectors(db, vectorFile,
                                                      tsneClassification,
                                                      eliminateOrphans, settings.unwrapMglu)

   numSamples = len(yOrig)
   nClasses = len(set(yOrig))
   if usePca:
      pca = PCA(n_components=10)
      XOrig = pca.fit_transform(XOrig)


   clf_orig = OneVsOneClassifier(svm.SVC(kernel="rbf"))
   # clf_orig = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(20, 10), random_state=1)

   clf = gridSearch(clf_orig, XOrig, yOrig)
   # clf = clf_orig

   clf = ExtendedOneVsOneClassifier(clf)

   K = nClasses*(nClasses-1)/2 # number of one-vs-one classifiers
   pred = np.zeros((numSamples, nClasses))
   votes = np.zeros((numSamples, nClasses))
   cfLevels = np.zeros((numSamples, K))

   for i in xrange(nRuns):
      X,y,ind = permuteData(XOrig,yOrig)

      kf = KFold(n_splits=nFolds)
      scores_all = [0]*nScores


      for train, test in kf.split(X):
         clf.fit(X[train], y[train])
         votesKf, cfLevelsKf = clf.getVotesAndCF(X[test])
         yPred = []

         # accumulate predictions, votes and confidence levels
         for j in xrange(len(test)):
            index_orig = ind[test[j]]
            predKf = votesKf[j].argmax()
            yPred.append(predKf)

            pred[index_orig][predKf] += 1
            votes[index_orig] += votesKf[j]
            cfLevels[index_orig] += cfLevelsKf[j]

         scores = compute_scores(y[test], yPred)
         for i in range(nScores):
            scores_all[i] += (scores[i])
         logger.info(scoresToString(scores, nScores, scoreNames))

   mc = computeMc(names, yOrig, pred, votes, cfLevels)

   logger.info("Proteins with error rate > %d%%:\n%s" % (mcRateThreshold *100,
         ('\n'.join([mcToString(x, yDictReverse) for x in mc]))))
   return mc, yDictReverse

if __name__ == "__main__":
   mc = []
   mc_names = []
   for k in xrange(2):
      res, yDictReverse = doClassification(modelNames[k], vectorFiles[k])
      # sort by names
      res = sortByFields(res, ['name'])
      mc.append(res)
      # extract names
      mc_names.append([x["name"] for x in res])

   # get intersections
   inters = []
   for k in xrange(2):
      inters.append(filter(lambda x: x["name"] in mc_names[(k+1)%2], mc[k]))

   logger.info('\nIntersection of misclassifications')
   logger.info('\n\n'.join([mcToString(x[0], yDictReverse) + '\n' +
                            mcToString(x[1], yDictReverse)
                            for x in zip(inters[0], inters[1])]))