import math
import sklearn.metrics as metrics

def multiclassMCC(cm):
   '''
   computes matthews correlation for multiclass case
   as described in Gorodkin's paper http://www.sciencedirect.com/science/article/pii/S1476927104000799
   :param cm: confusion matrix
   :return:
   '''
   K = len(cm)
   numerator = 0
   for k in range(K):
      for l in range(K):
         for m in range(K):
            numerator += (cm[k][k]*cm[l][m] - cm[k][l]*cm[m][k])
   denominator1 = 0
   for k in range(K):
      m1 = 0
      for l in range(K):
         m1+= cm[k][l]
      m2 = 0
      for k2 in range(K):
         if k2!=k:
            for l2 in range(K):
               m2+=cm[k2][l2]

      denominator1+=m1*m2
   denominator1 = math.sqrt(denominator1)

   denominator2 = 0
   for k in range(K):
      m1 = 0
      for l in range(K):
         m1+= cm[l][k]
      m2 = 0
      for k2 in range(K):
         if k2!=k:
            for l2 in range(K):
               m2+=cm[l2][k2]

      denominator2+=m1*m2
   denominator2 = math.sqrt(denominator2)
   if denominator1*denominator2 == 0:
      return 0.0
   val = numerator/(denominator1*denominator2)
   return val

def scoresToString(scores, numScores, scoreNames):
   s = ''
   for i in range(numScores):
      s += ('%s = %.4f, ' % (scoreNames[i],scores[i]))
   return s

def scoresToStringSubcl(scores, numScores, scoreNames):
   s = ''
   for i in range(numScores):
      if isinstance(scores[i], float):
         s += ('%s = %.4f, ' % (scoreNames[i],scores[i]))
      else:
         s += ('%s = %s, ' % (scoreNames[i],','.join(["%.4f"%k for k in scores[i]])))
   return s


def confIntToString(confInt, numScores, scoreNames):
   s = ''
   for i in range(numScores):
      s += ('%s = %.4f (%.4f,%.4f), ' % (scoreNames[i],confInt[i][0],
                                         confInt[i][0] +confInt[i][1],
                                         confInt[i][0] - confInt[i][1]))
   return s

def confIntToReportString(confInt, numScores):
   s = ''
   for i in range(numScores):
      s += "%.4f (%.4f,%.4f) & " % (confInt[i][0],
                                         confInt[i][0] - confInt[i][1],
                                         confInt[i][0] + confInt[i][1])
   return s

def scoresToReportString(scores, numScores):
   forRep = ''
   for i in range(numScores):
      forRep += ('%.4f & ' % (scores[i]))
   return forRep

def compute_scores(y_true, y_pred):
   # confusion matrix: sum of elements in each row is equal
   # to the number of corresponding true labels
   cm = metrics.confusion_matrix(y_true, y_pred)
   matthews_corrcoef = multiclassMCC(cm)
   return [matthews_corrcoef,
           metrics.accuracy_score(y_true, y_pred),
           metrics.precision_score(y_true, y_pred, average='weighted'),
           metrics.recall_score(y_true, y_pred, average='weighted')]

def compute_scores_subcl(y_true, y_pred, n):
   # confusion matrix: sum of elements in each row is equal
   # to the number of corresponding true labels
   cm = metrics.confusion_matrix(y_true, y_pred)
   matthews_corrcoef = multiclassMCC(cm)
   f1 = metrics.f1_score(y_true, y_pred, average="weighted")
   mcc_subcl = []
   classes = xrange(n)
   for cl in classes:
      mcc_subcl.append(metrics.matthews_corrcoef([1 if y == cl else 0 for y in y_true],
                          [1 if y == cl else 0 for y in y_pred]))
   return [metrics.accuracy_score(y_true, y_pred),
           matthews_corrcoef,
           f1,
           metrics.precision_score(y_true, y_pred, labels=classes, average=None),
           metrics.recall_score(y_true, y_pred, labels=classes, average=None),
           metrics.f1_score(y_true, y_pred, labels=classes, average=None),
           mcc_subcl]

def computeAvgs(metrics, n):
   res = []
   for m in metrics:
      if isinstance(m[0], float):
         res.append(sum(m)/float(len(m)))
      else:
         m_avg = []
         for i in xrange(len(m[0])):
            mm_avg = 0
            for mm in m:
               mm_avg += mm[i]
            m_avg.append(mm_avg/float(len(m)))
         res.append(m_avg)
   return res