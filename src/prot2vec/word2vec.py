import gensim
import requests
from src.utils import settings


class MySequences(object):

   ngramSize = 3 # trigram model

   root = "http://gpcrdb.org/services/"
   familyUrl = 'proteinfamily/'
   childrenUrl = 'children/'
   proteinUrl = 'proteins/'
   rootFamily = '000'

   def __init__(self):
      self.families = []
      self.currentFamily = None
      self.proteins = []
      response = requests.get(self.root + self.familyUrl + self.childrenUrl + self.rootFamily +  "/")
      families = response.json()
      for family in families:
         self.families.append(family['slug'])

   def splitWithOffset(self, seq, offset):
      res = []
      length = len(seq)
      for i in xrange(offset, length, self.ngramSize):
         if i + self.ngramSize <= length:
            res.append(seq[i:i+self.ngramSize])
      return res

   def retrieveProteins(self):
      self.proteins = []
      response = requests.get(self.root + self.familyUrl + self.proteinUrl + self.currentFamily + "/")
      proteinData = response.json()
      for protein in proteinData:
         self.proteins.append(protein['sequence'])


   def __iter__(self):
      for family in self.families:
         self.currentFamily = family
         self.retrieveProteins()
         for seq in self.proteins:
            for i in xrange(self.ngramSize):
               yield self.splitWithOffset(seq, i)


if __name__=="__main__":
   sentences = MySequences() # a memory-friendly iterator

   model = gensim.models.Word2Vec(sentences, sg=1, min_count=2, window=25)
   model.save(settings.currentDb.modelFilename)