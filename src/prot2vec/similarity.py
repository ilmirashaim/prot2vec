from sklearn.metrics.pairwise import cosine_similarity
from proteinsToVectors import TrigramDict

dict = TrigramDict()
trigramDict = dict.getSwissprotDict()
similar = []
for x in trigramDict.keys():
  if x!='AAA':
     similar.append((x, cosine_similarity(trigramDict['AAA'], trigramDict.get(x))))
sorted(similar, key=lambda tup: abs(tup[1]), reverse=True)


