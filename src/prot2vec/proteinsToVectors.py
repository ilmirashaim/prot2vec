import csv
from operator import add
import gensim
from src.utils import settings

db = settings.db
class ProteinProcessor(object):

    def __init__(self, db=settings.db, eliminateOrphans = False, unwrapMglur = False):
        self.names = []
        self.families = []
        self.trigramsDict = {}
        self.sequences = []
        self.proteinFilename =db.proteinsFilename
        self.db = db

        res = db.getProteins(['sequence', 'name', 'family'],
                          unwrapMglur=unwrapMglur,
                          eliminateOrphans= eliminateOrphans)

        self.sequences = res['sequence']
        self.names = res['name']
        self.families = res['family']

    def getSequenceVectors(self, trigramsDict, ngramSize = 3):
        defaultVector = [0]*100
        seqVectors = []
        totalN = 0
        notFound = 0

        for sequence in self.sequences:
            seqVector = defaultVector
            for j in xrange(len(sequence) - ngramSize + 1):
                vec = trigramsDict.get(sequence[j:j+ngramSize], defaultVector)
                if all(vec[i] == defaultVector[i] for i in range(len(vec))):
                    notFound+=1
                totalN +=1
                seqVector = map(add, seqVector, vec)
            seqVectors.append(seqVector)
        print "Number of not found %d out of %d" % (notFound, totalN)
        return seqVectors


class TrigramDict(object):

    def getSwissprotDict(self):
        trigramsDict = {}

        with open(settings.currentDb.trigramFilename, 'rb') as csvfile:
            for row in csvfile:
                vector = row.replace('"', "").split()
                trigram = vector[0]
                vector = vector[1:]
                trigramsDict[trigram] = [float(x) for x in vector]
        return trigramsDict

    def getGpcrdbDict(self):
        trigramsDict = {}

        model = gensim.models.Word2Vec.load(settings.currentDb.modelFilename)
        keys = model.vocab.keys()
        for k in keys:
            trigramsDict[k] = model[k]

        return trigramsDict

if __name__ == "__main__":

    dict = TrigramDict()
    proteinProcessor = ProteinProcessor(db)
    dicts = [dict.getSwissprotDict(),dict.getGpcrdbDict()]
    vectorFiles = [settings.db.swissprotVectorsFilename, settings.db.gpcrdbVectorsFilename]
    ngramSizes = [3, 3]
    for i in [0,1]:
        seqVectors = proteinProcessor.getSequenceVectors(dicts[i], ngramSizes[i])

        with open(vectorFiles[i], 'w') as csvfile:
            writer = csv.writer(csvfile, lineterminator = '\n',  delimiter='\t')
            for seqVector in seqVectors:
                writer.writerow(seqVector)





