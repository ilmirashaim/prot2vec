import csv
import gensim
from src.utils import settings

db = settings.newDb
class ProteinProcessor(object):

    def __init__(self, db=settings.db, eliminateOrphans = False, unwrapMglur = False, ngramSize = 3):
        self.names = []
        self.families = []
        self.trigramsDict = {}
        self.sequences = []
        self.ngramSize = ngramSize
        self.proteinFilename =db.proteinsFilename
        self.db = db

        res = db.getProteins(['sequence', 'name', 'family'],
                          unwrapMglur=unwrapMglur,
                          eliminateOrphans= eliminateOrphans)

        self.sequences = res['sequence']
        self.names = res['name']
        self.families = res['family']
        self.model = gensim.models.Doc2Vec.load(settings.currentDb.modelDoc2vecFilename)

    def split(self, seq):
      res = []
      length = len(seq)
      for i in xrange(0, length, self.ngramSize):
         if i + self.ngramSize <= length:
            res.append(seq[i:i+self.ngramSize])
      return res

    def getSequenceVectors(self):
        seqVectors = []

        for sequence in self.sequences:
            seqVector = self.model.infer_vector(self.split(sequence))
            seqVectors.append(seqVector)
        return seqVectors



if __name__ == "__main__":

    proteinProcessor = ProteinProcessor(db)
    vectorFile = db.doc2vecVectorsFilename
    seqVectors = proteinProcessor.getSequenceVectors()

    with open(vectorFile, 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator = '\n',  delimiter='\t')
        for seqVector in seqVectors:
            writer.writerow(seqVector)





