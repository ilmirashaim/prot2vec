import gensim
import requests
from src.utils import settings


class MySequences(object):

   ngramSize = 3 # trigram model

   root = "http://gpcrdb.org/services/"
   familyUrl = 'proteinfamily/'
   childrenUrl = 'children/'
   proteinUrl = 'proteins/'
   rootFamily = '000'

   def __init__(self, tokens_only=False):
      self.families = []
      self.currentFamily = None
      self.proteins = []
      self.tokens_only = tokens_only
      response = requests.get(self.root + self.familyUrl + self.childrenUrl + self.rootFamily +  "/")
      families = response.json()
      for family in families:
         self.families.append(family['slug'])

   def split(self, seq):
      res = []
      length = len(seq)
      for i in xrange(0, length, self.ngramSize):
         if i + self.ngramSize <= length:
            res.append(seq[i:i+self.ngramSize])
      return res

   def retrieveProteins(self):
      self.proteins = []
      response = requests.get(self.root + self.familyUrl + self.proteinUrl + self.currentFamily + "/")
      proteinData = response.json()
      for protein in proteinData:
         self.proteins.append(protein['sequence'])


   def __iter__(self):
      for family in self.families:
         self.currentFamily = family
         self.retrieveProteins()
         for i, seq in enumerate(self.proteins):
            if self.tokens_only:
                yield self.split(seq)
            else:
                # For training data, add tags
                yield gensim.models.doc2vec.TaggedDocument(self.split(seq), [i])


sentences = MySequences() # a memory-friendly iterator
model = gensim.models.doc2vec.Doc2Vec(sentences, size=100, dm=1, min_count=2, window=25)
model.save(settings.currentDb.modelDoc2vecFilename)