import csv

from sklearn.manifold import TSNE

from src.prot2vec.proteinsToVectors import ProteinProcessor
from src.prot2vec.proteinsToVectors import TrigramDict
from src.utils import settings

db = settings.db
unwrapMglur = settings.unwrapMglu
eliminateOrphans = settings.elimOrphans

proteinProcessor = ProteinProcessor(db, unwrapMglur=unwrapMglur, eliminateOrphans=eliminateOrphans)
dict = TrigramDict()
vectorFiles = [settings.db.swissprotVectorsTsneFilename, settings.db.gpcrdbVectorsTsneFilename]
familyLabelsFilename = settings.db.familyLabelsFilename
dicts = [dict.getSwissprotDict(), dict.getGpcrdbDict()]


for i in [0,1]:
   seqVectors = proteinProcessor.getSequenceVectors(dicts[i])

   tsneModel = TSNE(n_components=2, random_state=0)
   transformed = tsneModel.fit_transform(seqVectors)
   with open(vectorFiles[i], 'w') as csvfile:
       writer = csv.writer(csvfile, lineterminator = '\n',  delimiter='\t')
       for seqVector in transformed:
           writer.writerow(seqVector)
   print("vector file '%s' created" % vectorFiles[i])

with open(familyLabelsFilename, 'w') as csvfile:
   for family in proteinProcessor.families:
      csvfile.write(str(family) + "\n")
print("family labels file '%s' created" % familyLabelsFilename)
