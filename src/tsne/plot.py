import numpy as np
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from src.utils import settings

db = settings.db
unwrapMglu = settings.unwrapMglu
elimOrphans = settings.elimOrphans


modelNames = settings.db.modelNames
filenames = [settings.db.swissprotVectorsTsneFilename, settings.db.gpcrdbVectorsTsneFilename]
familyLabelsFilename = settings.db.familyLabelsFilename
namesDict = settings.db.getFamiliesDict()
if db == settings.oldDbLab:
   plotNames = [settings.db.figureFileTemplate %
                (modelName.lower(),'_withorph' if not elimOrphans else '')
                for modelName in modelNames]
else:
   plotNames = [settings.db.figureFileTemplate %
                (modelName.lower(), settings.settingsTupleStr)
                for modelName in modelNames]

labels = np.recfromcsv(familyLabelsFilename, names=['l'])
names = set(labels['l'])
names = sorted(list(names))
namesArranged = []
for name in names:
   if namesDict[name].startswith('mGlu'):
      namesArranged.append(name)
for name in names:
   if not namesDict[name].startswith('mGlu'):
      namesArranged.append(name)



colorsForFamilies = {
   'GABA<sub>B</sub> receptors':'r',
   'Metabotropic glutamate receptors':'g',
   'Taste 1 receptors': 'b',
   'Calcium-sensing receptors': (1,1,0),
   'Class C Orphans': 'w',
   'mGlu<sub>1</sub> receptor':'#90fefb',
   'mGlu<sub>2</sub> receptor':'#68838B',
   'mGlu<sub>3</sub> receptor':(0.0, 0.4, 0.0),
   'mGlu<sub>4</sub> receptor':(0.7, 1.0, 0.2),
   'mGlu<sub>5</sub> receptor':(0.0, 1.0, 0.5),
   'mGlu<sub>6</sub> receptor':(0.12, 0.7, 0.6),
   'mGlu<sub>7</sub> receptor':'#B9D3EE',
   'mGlu<sub>8</sub> receptor':'#007FFF',
   'MGlu': 'g',
   'CS': (1,1,0),
   'GB': 'r',
   'VN': (1,0,1),
   'Ph': (0.3, 0, 0.5),
   'Od': (0.8, 0.7, 0.5),
   'Ta': 'b',
   'Orph': 'w'
}

for i in xrange(2):

   data = np.recfromcsv(filenames[i],delimiter = '\t', names= ['x','y'])

   x,y = data['x'],  data['y']
   fig = plt.figure(figsize=(10,7))
   ax = plt.gca()

   for name in namesArranged:
       cond = labels['l'] == name
       colorVal = colorsForFamilies[namesDict[name]]
       ax.plot(x[cond], y[cond], linestyle='none', marker='o', label=namesDict[name], color=colorVal)

   fontP = FontProperties()
   fontP.set_size('small')
   lgd = ax.legend(numpoints=1, prop=fontP, loc='upper center', bbox_to_anchor=(0.5, -0.05), ncol=3)

   ax.set_title("Protein families for projections from %s trained word2vec" % modelNames[i])
   fig.savefig(plotNames[i], orientation ='landscape', bbox_extra_artists=(lgd,), bbox_inches='tight')
   print('file %s created' % plotNames[i])