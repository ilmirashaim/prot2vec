import csv
from src.utils import settings

db = settings.db
class ProteinProcessor(object):
    alphabet = ['A', 'C', 'E', 'D', 'G', 'F', 'I', 'H', 'K', 'M', 'L', 'N', 'Q', 'P', 'S', 'R', 'T', 'W', 'V', 'Y']
    def __init__(self, db=settings.db, eliminateOrphans = False, unwrapMglur = False):
        self.names = []
        self.families = []
        self.trigramsDict = {}
        self.sequences = []
        self.proteinFilename =db.proteinsFilename
        self.db = db

        res = db.getProteins(['sequence', 'name', 'family'],
                          unwrapMglur=unwrapMglur,
                          eliminateOrphans= eliminateOrphans)

        self.sequences = res['sequence']
        self.names = res['name']
        self.families = res['family']

    def getSequenceVectors(self):
        seqVectors = []

        for sequence in self.sequences:
            seqVector = []
            i = 0
            for a1 in self.alphabet:
                for a2 in self.alphabet:
                    seqVector.append(sequence.count(a1+a2))
                    i+=1
            seqVectors.append(seqVector)
        return seqVectors


if __name__ == "__main__":

    proteinProcessor = ProteinProcessor(db)

    vectorFile = settings.db.aacVectorsFilename

    seqVectors = proteinProcessor.getSequenceVectors()
    with open(vectorFile, 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator = '\n',  delimiter='\t')
        for seqVector in seqVectors:
            writer.writerow(seqVector)





