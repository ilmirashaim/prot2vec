import numpy as np
from pkg_resources import resource_filename
import logging
import random
from read.familiesDict import getFamiliesDict, getOldFamiliesDict
from read.proteins import getProteins, getOldProteins

class Config():
    def __init__(self):
        with open(resource_filename('resources', 'config')) as config:
            self.config = {}
            for l in config:
                paramParts = [x.strip() for x in l.split('=')]
                val = paramParts[1]
                if val == 'False':
                    val = False
                elif val == 'True':
                    val = True
                self.config[paramParts[0]] = val

    def __getitem__(self, i):
        return self.config[i]

class DbSettings():
   def __init__(self, name, settings):
       settingsTupleStr = settings.settingsTupleStr

       self.name = name
       self.modelNames = ["SWISSPROT", "GPCRDB"]
       suffix = ('' if name==settings.currentDbLab else '.' + name)
       self.dataResource = "resources.data" + suffix
       self.resultsResource = "resources.results" + suffix
       self.logsResource = "resources.logs" + suffix
       self.figuresResource = "resources.results.figures" + suffix

       if name == settings.oldDbLab:
           self.orphanLabel = 8
           self.dbFile = resource_filename(self.dataResource, 'GPCR_Sequences.mat')
           self.dbFilteredFile = resource_filename(self.dataResource, 'dataset1252_sequence.csv')
           self.proteinsFilename = ''
       else:
           self.orphanLabel = '004_004_001'
           self.mglurLabel = '004_002_002'
           self.proteinsFilename = resource_filename(self.dataResource, 'proteins.csv')
           self.familiesFilename = resource_filename(self.dataResource, 'families.csv')

       self.figureFileTemplate = resource_filename(self.figuresResource, '%s%s%s.png')
       self.modelFilename = resource_filename(self.resultsResource, 'model')
       self.familyLabelsFilename = resource_filename(self.dataResource, 'familyLabels%s%s.csv' % settingsTupleStr)
       self.swissprotVectorsFilename = resource_filename(self.resultsResource, 'vectors.csv')
       self.gpcrdbVectorsFilename = resource_filename(self.resultsResource, 'vectorsGPCRDB.csv')
       self.aacVectorsFilename = resource_filename(self.resultsResource, 'vectorsAAC.csv')
       self.aaVectorsFilename = resource_filename(self.resultsResource, 'vectorsAA.csv')
       self.digramVectorsFilename = resource_filename(self.resultsResource, 'vectorsDigram.csv')
       self.accVectorsFilename = resource_filename(self.resultsResource, 'vectorsACC.csv')
       self.zscoreFilename = resource_filename(self.dataResource, 'z-score.csv')

       self.swissprotVectorsTsneFilename = resource_filename(self.resultsResource,
                                                              'vectorsTsne%s%s.csv' % settingsTupleStr)
       self.gpcrdbVectorsTsneFilename = resource_filename(self.resultsResource,
                                                           'vectorsGPCRDBTsne%s%s.csv' % settingsTupleStr)
       self.classificationResFilename = resource_filename(self.resultsResource,
                                                          'classificationRes%s%s%s.txt' %
                                                          (settingsTupleStr[0], settingsTupleStr[1],
                                                           '_tsne' if settings.tsneClassification else ''))
       self.classificationResRFFilename = resource_filename(self.resultsResource,
                                                          'classificationRF%s%s%s.txt' %
                                                          (settingsTupleStr[0], settingsTupleStr[1],
                                                           '_tsne' if settings.tsneClassification else ''))

       self.ptResFilename = resource_filename(self.dataResource, 'ptRes.txt')
       self.ptResAnnotatedFilename = resource_filename(self.dataResource, 'ptResAnnotated.txt')
       self.fastaFilename = resource_filename(self.dataResource, 'proteins.fa')
       self.compareResFilename = resource_filename(self.dataResource, 'compare.txt')
       self.trigramFilename = resource_filename(self.dataResource, 'protVec_100d_3grams.csv')
       self.logsDir = resource_filename(self.logsResource, '')
       self.logsMetadata = resource_filename(self.logsResource, 'metadata.tsv')
       self.kernelGlobal = resource_filename(self.resultsResource, 'kernelGlobal')
       self.confInts = resource_filename(self.resultsResource, 'confInts')

   def getFamiliesDict(self):
      if self.name == "old":
         return getOldFamiliesDict()
      else:
         return getFamiliesDict(self.familiesFilename)

   def getProteins(self, names = None,
                eliminateOrphans = False,
                unwrapMglur = False):

      if self.name == "old":
         return getOldProteins(self.dbFile, eliminateOrphans, self.orphanLabel)
      else:
         return getProteins(self.proteinsFilename, names,
                            eliminateOrphans, self.orphanLabel,
                            unwrapMglur, self.mglurLabel)

class Settings():
    def __init__(self):
       self.config = Config()
       self.unwrapMglu = self.config['unwrapMglu']
       self.elimOrphans = self.config['elimOrphans']
       self.tsneClassification = self.config['tsneClassification']
       self.pca = self.config['pca']

       self.currentDbLab = 'cur'
       self.oldDbLab = 'old'
       self.newDbLab = 'new'
       self.settingsTupleStr = ('_unwrap' if self.unwrapMglu else '', '_withorph' if not self.elimOrphans else '')
       self.oldDb = DbSettings(self.oldDbLab, self)
       self.newDb = DbSettings(self.newDbLab, self)
       self.currentDb = DbSettings(self.currentDbLab, self)

       if self.config['db'] == self.currentDbLab:
           self.db = self.currentDb
       elif self.config['db'] == self.oldDbLab:
           self.db = self.oldDb
       else:
           self.db = self.newDb
       self.logsTrigramDir = resource_filename(self.currentDb.logsResource, 'trigram')

       self.colors = {
   'GABA<sub>B</sub> receptors':'r',
   'Metabotropic glutamate receptors':'g',
   'Taste 1 receptors': 'b',
   'Calcium-sensing receptors': 'y',
   'Class C Orphans': 'w',
   'mGlu<sub>1</sub> receptor':'#90fefb',
   'mGlu<sub>2</sub> receptor':'#68838B',
   'mGlu<sub>3</sub> receptor':(0.0, 0.4, 0.0),
   'mGlu<sub>4</sub> receptor':(0.7, 1.0, 0.2),
   'mGlu<sub>5</sub> receptor':(0.0, 1.0, 0.5),
   'mGlu<sub>6</sub> receptor':(0.12, 0.7, 0.6),
   'mGlu<sub>7</sub> receptor':'#B9D3EE',
   'mGlu<sub>8</sub> receptor':'#007FFF',
   'MGlu': 'g',
   'CS': (1,1,0),
   'GB': 'r',
   'VN': (1,0,1),
   'Ph': (0.3, 0, 0.5),
   'Od': (0.8, 0.7, 0.5),
   'Ta': 'b',
   'Orph': 'w'
}

settings = Settings()



def initLogger(filename):
   print('init logger in %s' % filename)
   logger = logging.getLogger()
   logger.setLevel(logging.DEBUG)
   formatter = logging.Formatter('%(asctime)s - %(message)s')
   fh = logging.FileHandler(filename, mode='w')
   fh.setLevel(logging.DEBUG)
   fh.setFormatter(formatter)
   logger.addHandler(fh)

   ch = logging.StreamHandler()
   ch.setLevel(logging.DEBUG)
   ch.setFormatter(formatter)
   logger.addHandler(ch)
   return logger


def permuteData(X, y, seed=123):
   ind = range(len(X))
   random.seed(seed)
   random.shuffle(ind)

   X_new = np.array([X[i] for i in ind])
   y_new = np.array([y[i] for i in ind])
   return X_new, y_new, ind



