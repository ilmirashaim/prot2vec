import tensorflow as tf
from src.utils import settings
from tensorflow.contrib.tensorboard.plugins import projector
import os

names = []
values = []
with open(settings.currentDb.trigramFilename, 'rb') as csvfile:
   for row in csvfile:
       vector = row.replace('"', "").split()
       trigram = vector[0]
       vector = vector[1:]
       values.append([float(x) for x in vector])
       names.append(trigram)

metadataPath = os.path.join(settings.logsTrigramDir, 'metadata.tsv')

with open(metadataPath, 'w') as metadataFile:
   # metadataFile.write("Names\n")
   for n in names[:-1]:
      metadataFile.write(n)
      metadataFile.write("\n")
   metadataFile.write(names[-1])

embedding_var = tf.Variable(values, name="3_grams_AA")
init = tf.global_variables_initializer()

with tf.Session() as session:
   session.run(init)
   saver = tf.train.Saver()
   saver.save(session, os.path.join(settings.logsTrigramDir, "model.ckpt"))

summary_writer = tf.train.SummaryWriter(settings.logsTrigramDir)
config = projector.ProjectorConfig()

embedding = config.embeddings.add()
embedding.tensor_name = embedding_var.name
embedding.metadata_path = metadataPath

projector.visualize_embeddings(summary_writer, config)




