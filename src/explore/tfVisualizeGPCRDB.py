import tensorflow as tf
from src.utils import settings
from tensorflow.contrib.tensorboard.plugins import projector
import os
from src.read.vectors import getVectors

print (settings.db.gpcrdbVectorsFilename)
values = getVectors(settings.db.aacVectorsFilename)

res = settings.db.getProteins(['family', 'entry_name'])

familiesDict=settings.db.getFamiliesDict()
names = res['entry_name']
families = res['family']

with open(settings.db.logsMetadata, 'w') as metadataFile:
   metadataFile.write("Name\tFamily\n")
   for i in xrange(len(names)):
      metadataFile.write(names[i])
      metadataFile.write("\t")
      metadataFile.write(familiesDict[families[i]])
      metadataFile.write("\n")

embedding_var = tf.Variable(values, name="proteins")
init = tf.global_variables_initializer()

with tf.Session() as session:
   session.run(init)
   saver = tf.train.Saver()
   saver.save(session, os.path.join(settings.db.logsDir, "model.ckpt"))

summary_writer = tf.train.SummaryWriter(settings.db.logsDir)
config = projector.ProjectorConfig()

embedding = config.embeddings.add()
embedding.tensor_name = embedding_var.name
embedding.metadata_path = settings.db.logsMetadata

projector.visualize_embeddings(summary_writer, config)




