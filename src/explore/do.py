from src.utils import settings
from src.read.familiesDict import getFamiliesDict
import matplotlib.pyplot as plt
colorsForFamilies = {
   'GABA_B receptors':'r',
   'Metabotropic glutamate receptors':'g',
   'Taste 1 receptors': 'b',
   'Calcium-sensing receptors': (1,1,0),
   'Class C Orphans': 'w',
   'mGlu_1 receptor':'#90fefb',
   'mGlu_2 receptor':'#68838B',
   'mGlu_3 receptor':(0.0, 0.4, 0.0),
   'mGlu_4 receptor':(0.7, 1.0, 0.2),
   'mGlu_5 receptor':(0.0, 1.0, 0.5),
   'mGlu_6 receptor':(0.12, 0.7, 0.6),
   'mGlu_7 receptor':'#B9D3EE',
   'mGlu_8 receptor':'#007FFF',
   'MGlu': 'g',
   'CS': (1,1,0),
   'GB': 'r',
   'VN': (1,0,1),
   'Ph': (0.3, 0, 0.5),
   'Od': (0.8, 0.7, 0.5),
   'Ta': 'b',
   'Orph': 'w'
}


db = settings.db

def drawPie(labelsNames, sizes, colors=None):
   if colors is None:
      colors = [colorsForFamilies[x] for x in labelsNames]
   total = sum(sizes)

   plt.pie(sizes, labels=labelsNames, colors = colors,
            autopct=lambda(p): "%.0f\n%1.1f%%"%(p * total / 100, p),
           shadow=True, startangle=90)
   # Set aspect ratio to be equal so that pie is drawn as a circle.
   plt.axis('equal')
   plt.show()

def processLabels(yLabels):
   labels = list(set(yLabels))
   labelsDict = getFamiliesDict(settings.db.familiesFilename)
   sizes = [yLabels.count(a) for a in labels]
   labelsNames = [labelsDict[a] for a in labels]
   labelsNames = ['AAA' + l if l.startswith('mGlu') or l.startswith('Meta') else l for l in labelsNames]

   s = sorted(zip(labelsNames,sizes))
   sizes = [x for (y,x) in s]
   labelsNames = [y for (y,x) in s]

   labelsNames = [l.replace('AAA', '').replace('<sub>', '_').replace('</sub>', '') for l in labelsNames]

   drawPie(labelsNames, sizes)

def drawAll():

   res = db.getProteins(['family', 'accession'], unwrapMglur=False,
                           eliminateOrphans=False)

   yLabels = res['family']
   print(len(yLabels))
   processLabels(yLabels)

def drawMglur():
   res = db.getProteins(['family', 'accession'], unwrapMglur=True,
                           eliminateOrphans=False)
   yLabels = res['family']
   processLabels(yLabels)

if __name__ == "__main__":

   drawAll()
   drawMglur()
