import csv
from src.utils import settings
import math

db = settings.db
class ProteinProcessor(object):
    alphabet = ['A', 'C', 'E', 'D', 'G', 'F', 'I', 'H', 'K', 'M', 'L', 'N', 'Q', 'P', 'S', 'R', 'T', 'W', 'V', 'Y']
    d = 5

    def __init__(self, p, L, db=settings.db, eliminateOrphans = False, unwrapMglur = False):
        self.names = []
        self.families = []
        self.trigramsDict = {}
        self.sequences = []
        self.proteinFilename =db.proteinsFilename
        self.db = db

        res = db.getProteins(['sequence', 'name', 'family'],
                          unwrapMglur=unwrapMglur,
                          eliminateOrphans= eliminateOrphans)

        self.sequences = res['sequence']
        self.names = res['name']
        self.families = res['family']

        self.zscore = {}
        self.p = p
        self.L = L

        with open(settings.db.zscoreFilename, 'r') as zscorefile:
            csvreader = csv.reader(zscorefile, delimiter=' ')
            i = 0
            sums = [0 for j in xrange(self.d)]
            for val in csvreader:
                try:
                    self.zscore[self.alphabet[i]] = [float(x) for x in val]
                    for j in xrange(self.d):
                        sums[j] += self.zscore[self.alphabet[i]][j]
                except Exception as e:
                    print i
                    print e
                i+=1
            self.zscore['X'] = [s/float(len(self.alphabet)) for s in sums]

    def npDot(self, v1, v2):
        if len(v1) != len(v2):
            raise Exception( 'error, vectors of diff lengths %d and %d' % (len(v1), len(v2)))
        return sum(v1[i] * v2[i] for i in xrange(len(v1)))

    def getSequenceVectors(self):
        seqVectors = []

        for sequence in self.sequences:
            seqVector = []
            n = len(sequence)
            zscoreVector = [[] for i in xrange(n)]

            for aa in sequence:
                zscore = self.zscore[aa]
                for i in xrange(self.d):
                    zscoreVector[i].append(zscore[i])

            # normalize zscores
            for i in xrange(self.d):
                mean = sum(zscoreVector[i])/float(n)
                zscoreVector[i] = [x - mean for x in zscoreVector[i]]

            # iterate over lag
            for l in xrange(1, self.L+1):
                # compute autocovariances
                for i in xrange(self.d):
                    res = self.npDot(zscoreVector[i][:-l] ,
                                     zscoreVector[i][l:])/\
                          float(math.pow(n-l, self.p))
                    seqVector.append(res)
                # compute cross-covariances
                for i in xrange(self.d-1):
                    for j in xrange(i+1, self.d):
                        res = self.npDot(zscoreVector[i][:-(n-l)] , zscoreVector[j][n-l:])/\
                          float(math.pow(n-l, self.p))
                        seqVector.append(res)

            seqVectors.append(seqVector)
        return seqVectors


if __name__ == "__main__":

    proteinProcessor = ProteinProcessor(0.5, 13, db)

    vectorFile = settings.db.accVectorsFilename

    seqVectors = proteinProcessor.getSequenceVectors()
    with open(vectorFile, 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator = '\n',  delimiter='\t')
        for seqVector in seqVectors:
            writer.writerow(seqVector)





